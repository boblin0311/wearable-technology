import { ItemDecoration, Menu } from '../Menu';
export class LinkExchangeMenu extends Menu {
  public constructor(urlBase: string) {
    super(urlBase);
    this.addItem('艾利浩斯学院 图书馆', {
      button: true,
      link: 'http://ailihaosi.xyz/',
      decoration: ItemDecoration.ICON_LINK,
    });
    this.addItem('acted 咕咕喵的小说和小游戏', {
      button: true,
      link: 'https://acted.gitlab.io/h3/',
      decoration: ItemDecoration.ICON_LINK,
    });
    this.addItem('琥珀版可穿戴科技', {
      button: true,
      link: 'https://www.pixiv.net/novel/show.php?id=12189481',
      decoration: ItemDecoration.ICON_LINK,
    });
    this.addItem('零点之书', {
      button: true,
      link: 'https://zerono.page',
      decoration: ItemDecoration.ICON_LINK,
    });
  }
}