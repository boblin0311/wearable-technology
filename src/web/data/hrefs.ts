export function chapterHref(htmlRelativePath: string) {
  return `#/chapter/${htmlRelativePath}`;
}

export function pageHref(pageName: string) {
  return `#/page/${pageName}`;
}
