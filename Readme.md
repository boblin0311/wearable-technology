>《可穿戴科技》是一部重口味色情小说。因其中含有大量露骨的非常规性行为描写，本小说可能不适合所有年龄段，亦不宜在工作期间访问。

# 可穿戴科技 [![pipeline status](https://gitlab.com/SCLeo/wearable-technology/badges/master/pipeline.svg)](https://gitlab.com/SCLeo/wearable-technology/-/commits/master)
《可穿戴科技》是一部关于一名社会恐惧症美少女和某智能贞操带斗智斗勇的色情小说。

## [点此在线阅读 (GitLab Pages)](https://wt.tepis.me/)
- [镜像站 1 | wt.bgme.me（感谢 Shrink 提供）](https://wt.bgme.me)
- [镜像站 2 | rbq.desi（感谢 kookxiang 提供）](https://rbq.desi)
- [镜像站 3 | wt.makai.city（感谢友人♪b 提供）](https://wt.makai.city)
- [镜像站 4 | wt.0w0.bid（感谢立音喵提供）](https://wt.0w0.bid)
- [镜像站 5 | wt.umwings.com（感谢 C86 提供）](https://wt.umwings.com)

### 其他链接：[投稿](https://wt.tepis.me/#META/%E6%92%B0%E7%A8%BF%E9%A1%BB%E7%9F%A5%E5%8F%8A%E7%AE%80%E6%98%93-Markdown-%E6%95%99%E7%A8%8B.html) | [龙套](https://wt.tepis.me/#META/%E4%BA%BA%E5%90%8D%E8%AF%B7%E6%B1%82.html) | [WTCD 语言](https://wt.tepis.me/#META/WTCD/1.-%E6%A6%82%E8%BF%B0.html)
本小说[原先是放在 Google Docs 上](https://docs.google.com/document/d/1Pp5CtO8c77DnWGqbXg-3e7w9Q3t88P35FOl6iIJvMfo/edit?usp=sharing)的，但是由于篇幅变长，Google Docs 越来越卡，因此转移到 GitHub。然后 GitHub 又因为我们违反 ToS，[把我们的仓库封了](https://github.com/SCLeoX/Wearable-Technology)。

更新推送频道：https://t.me/joinchat/AAAAAEpkRVwZ-3s5V3YHjA

讨论组：https://t.me/joinchat/Dt8_WlJnmEwYNbjzlnLyNA

## 鸣谢
特别感谢以下小伙伴提供的建议与帮助，排名不分先后：

- [F74nk](https://t.me/F74nk_K)
- RainSlide
- 櫻川 紗良
- [幻梦](https://t.me/HuanmengQwQ)
- [VV](https://www.pixiv.net/users/58170013)
- 某不愿透露姓名的N性？
- 路人乙
- NekoCaffeine
- [acted咕咕喵](https://acted.gitlab.io/h3)
- [重水时雨](https://t.me/boatmasteronD2O)
- [TExL](http://texas.penguin-logistics.cn)
- 青葉
- [C86](https://c86.moe)
- 0 级骨钉
- 夏克斯
- 为霜
- 城岭 樱
- [零件](https://nekosc.com)
- kookxiang
- [Runian Lee](https://t.me/Runian)
- 不知名的N姓人士就好
- [帕蒂卡](https://github.com/Patika-ailemait)
- 神楽坂 紫
- kn
- 琥珀
- Skimige
- Testingdoll01
- 熵
- Butby
- 杨佳文
- lgd_小翅膀
- [pokemonchw](https://github.com/pokemonchw)
- 神楽坂 萌绫
- 王木木
- [友人♪B](https://zerono.page/%E5%85%B3%E4%BA%8E/)
- [czp](https://www.hiczp.com)
- 冰蓮音
- 立音喵

如果你想加入鸣谢列表里，你可以在讨论组或者在和作者的私聊里提提意见/脑洞啥的。哪怕你说的没有被采纳，我也十分愿意把你的名字加进来（一般情况下，如果我意识到了你在提意见，我就会主动来问你要不要加鸣谢列表。如果我没有意识到，我十分欢迎你主动来跟我说你要加鸣谢列表，热热闹闹的多好 √）。
